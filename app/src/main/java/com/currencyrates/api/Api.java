package com.currencyrates.api;

import com.currencyrates.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Api {

    private static final int TIMEOUT = 60;
    private static final int CONNECT_TIMEOUT = 10;

    private static final HttpLoggingInterceptor LOGGING_INTERCEPTOR = new HttpLoggingInterceptor();

    static {
        LOGGING_INTERCEPTOR.setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    private static final OkHttpClient CLIENT;

    static {
        CLIENT = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(LOGGING_INTERCEPTOR)
                .build();
    }

    private static Retrofit.Builder getRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(CLIENT)
                .addConverterFactory(GsonConverterFactory.create());
    }

    private static Gson getGson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    }

    public static CurrencyApi create() {
        return getRetrofitBuilder().client(CLIENT)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(CurrencyApi.class);
    }

    public interface CurrencyApi {
        @GET("latest")
        Observable<CurrencyModel> getRates(@Query("base") String base);
    }
}
