package com.currencyrates.api;

import android.support.annotation.NonNull;

import com.currencyrates.persistence.CurrencyRate;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class CurrencyModel {

    @NonNull
    @SerializedName("base")
    public String currencyName;

    @SerializedName("date")
    public Date date;

    @SerializedName("rates")
    public Map<String, Double> currencyRates;

    public List<CurrencyRate> normalizeRates() {
        List<CurrencyRate> rates = new ArrayList<>();
        for (String key : currencyRates.keySet()) {
            rates.add(new CurrencyRate(key, currencyRates.get(key)));
        }
        Collections.sort(rates, (currencyRate, currencyRate2) -> {
            return currencyRate.name.charAt(0) - currencyRate2.name.charAt(0);
        });
        rates.add(0, new CurrencyRate(currencyName, 1.00));
        return rates;
    }
}
