package com.currencyrates.repository;

import android.util.Log;

import com.currencyrates.api.Api;
import com.currencyrates.api.CurrencyModel;
import com.currencyrates.persistence.CurrencyDao;
import com.currencyrates.persistence.CurrencyRate;
import com.currencyrates.persistence.InputRate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CurrencyRepository {

    private CurrencyDao mCurrencyDao;

    private ConcurrentHashMap<String, Double> mCurrentRates = new ConcurrentHashMap<>();

    private InputRate mInputRate = new InputRate("EUR", 1.00);

    private CompositeDisposable disposable = new CompositeDisposable();

    public CurrencyRepository(CurrencyDao mCurrencyDao) {
        this.mCurrencyDao = mCurrencyDao;
        startPolling();
    }

    Observable<CurrencyModel> ratesApiObservable = Api.create().getRates("EUR")
            .doOnNext(currencyModel -> {

                mCurrentRates.clear();
                mCurrentRates.putAll(currencyModel.currencyRates);
                mCurrentRates.put("EUR", 1.00);
                List<CurrencyRate> list = currencyModel.normalizeRates();
                mCurrencyDao.insertCurrency(list);
            });

    public Flowable<List<CurrencyRate>> loadCurrency() {
        Flowable<List<CurrencyRate>> ratesDb = mCurrencyDao.getCurrencyRate()
                .map(currencyRates -> {
                    List<CurrencyRate> result = new ArrayList<>();
                    result.addAll(currencyRates);
                    Collections.sort(result, (currencyRate, currencyRate2) -> {
                        return currencyRate.name.charAt(0) - currencyRate2.name.charAt(0);
                    });

                    for (CurrencyRate rate : result) {
                        if ("EUR".equals(rate.name)) {
                            Log.d("!!!", "eur " + rate.value);
                            int position = result.indexOf(rate);
                            Collections.swap(result, 0, position);
                            break;
                        }
                    }
                    return result;
                });

        return ratesDb;
    }

    private void startPolling() {
        disposable.add(Observable.interval(1000, TimeUnit.MILLISECONDS)
                .timeInterval()
                .flatMap(t -> {
                    return ratesApiObservable;
                })
                .subscribeOn(Schedulers.io())
                .subscribe());
    }

    public void dispose() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable.clear();
    }

    public void insertInput(InputRate inputRate) {
        this.mInputRate = inputRate;
    }

    public InputRate getInput() {
        return mInputRate;
    }

    public Map<String, Double> getCurrentRates() {
        return mCurrentRates;
    }
}
