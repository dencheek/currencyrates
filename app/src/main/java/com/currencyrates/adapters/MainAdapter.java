package com.currencyrates.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.currencyrates.R;
import com.currencyrates.persistence.CurrencyRate;
import com.currencyrates.util.CircleTransform;
import com.currencyrates.util.CurrencyInfoUtil;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.CurrencyHolder> {
    public interface AdapterCallbacks {
        void onTextChanged(String base, Double k);
    }

    private Handler handler = new Handler();

    private List<CurrencyRate> currencyList;

    private AdapterCallbacks callbacks;

    public MainAdapter(AdapterCallbacks callbacks) {
        this.callbacks = callbacks;
        this.currencyList = new ArrayList<>();
    }

    @Override
    public CurrencyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new CurrencyHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrencyHolder holder, int position) {
        CurrencyRate currency = currencyList.get(position);
        holder.tvCurrencyName.setText(currency.name);
        if (!holder.etSum.isFocused()) {
            NumberFormat formatter = new DecimalFormat("#0.00");
            holder.etSum.setText(formatter.format(currency.value));
        }
        if (position == 0) {
            holder.etSum.setTag(R.id.position, position);
            holder.etSum.setTag(R.id.base, currency.name);
        }
        Picasso.with(holder.itemView.getContext())
                .load(CurrencyInfoUtil.getFlags().get(currency.name))
                .transform(new CircleTransform())
                .into(holder.ivFlag);
        holder.tvCurrencyCountry.setText(CurrencyInfoUtil.getNames().get(currency.name));
        holder.itemView.setOnClickListener(view -> {
            CurrencyRate from = currencyList.get(position);
            currencyList.remove(position);
            currencyList.add(0, from);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    notifyItemMoved(position, 0);
                }
            });
        });
    }

    @Override
    public long getItemId(int position) {
        return currencyList.get(position).name.hashCode();
    }

    public void setData(List<CurrencyRate> currencies) {
        currencyList.clear();
        currencyList.addAll(currencies);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }

    public class CurrencyHolder extends RecyclerView.ViewHolder {

        public ImageView ivFlag;
        public TextView tvCurrencyName;
        public TextView tvCurrencyCountry;
        public EditText etSum;

        public CurrencyHolder(View itemView) {
            super(itemView);
            ivFlag = itemView.findViewById(R.id.ivFlag);
            tvCurrencyName = itemView.findViewById(R.id.tvCurrencyName);
            tvCurrencyCountry = itemView.findViewById(R.id.tvCurrencyCountry);
            etSum = itemView.findViewById(R.id.etSum);
            etSum.addTextChangedListener(new CustomTextWatcher(etSum));
            etSum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean focused) {
                    if (focused) {
                        InputMethodManager imm = (InputMethodManager)
                                view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                        if (view.getParent() != null) {
                            ((ViewGroup) view.getParent()).performClick();
                        }
                    }
                }
            });
        }
    }

    private class CustomTextWatcher implements TextWatcher {

        private String currentText = "";

        private EditText view;

        private boolean isOnTextChanged;

        public CustomTextWatcher(EditText view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            currentText = charSequence.toString();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            isOnTextChanged = true;
            if ("".equals(charSequence.toString())) {
                try {
                    String base = (String) view.getTag(R.id.base);
                    callbacks.onTextChanged(base, 0.00);
                } catch (Exception e) {
                    Log.d("###", e.getMessage());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (isOnTextChanged) {
                isOnTextChanged = false;
                try {
                    int position = (int) view.getTag(R.id.position);
                    if (view.isFocused() && position == 0 && !currentText.equals(editable.toString())) {
                        String base = (String) view.getTag(R.id.base);
                        Double newSum = Double.parseDouble(editable.toString());
                        callbacks.onTextChanged(base, newSum);
                    }
                } catch (Exception e) {
                    Log.d("###", e.getMessage());
                }
            }

        }
    }
}
