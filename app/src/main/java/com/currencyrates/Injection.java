package com.currencyrates;

import android.content.Context;

import com.currencyrates.persistence.CurrencyDatabase;
import com.currencyrates.repository.CurrencyRepository;
import com.currencyrates.ui.ViewModelFactory;

public class Injection {

    private static CurrencyRepository REPO_INSTANCE;

    public static CurrencyRepository provideCurrencyDataSource(Context context) {
        if (REPO_INSTANCE == null) {
            CurrencyDatabase database = CurrencyDatabase.getInstance(context);
            REPO_INSTANCE = new CurrencyRepository(database.currencyDao());
        }
        return REPO_INSTANCE;
    }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        CurrencyRepository dataSource = provideCurrencyDataSource(context);
        return new ViewModelFactory(dataSource);
    }

}
