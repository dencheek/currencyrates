package com.currencyrates;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.currencyrates.adapters.MainAdapter;
import com.currencyrates.persistence.CurrencyRate;
import com.currencyrates.ui.CurrencyViewModel;
import com.currencyrates.ui.ViewModelFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class MainActivity extends AppCompatActivity implements MainAdapter.AdapterCallbacks {

    private RecyclerView mCurrenciesView;
    private ProgressBar progressBar;

    private ViewModelFactory mViewModelFactory;
    private CurrencyViewModel mViewModel;

    private MainAdapter adapter;

    private final CompositeDisposable mDisposable = new CompositeDisposable();

    LinearLayoutManager layoutManager = new LinearLayoutManager(this) {
        @Override
        public boolean supportsPredictiveItemAnimations() {
            return true;
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            super.onLayoutChildren(recycler, state);
            final int firstVisibleItemPosition = findFirstVisibleItemPosition();

            if (firstVisibleItemPosition == 0) {
                MainAdapter.CurrencyHolder holder = (MainAdapter.CurrencyHolder) mCurrenciesView.findViewHolderForLayoutPosition(0);
                if (!TextUtils.isEmpty(holder.etSum.getText())) {
                    holder.etSum.requestFocus();
                    holder.etSum.setSelection(holder.etSum.getText().length());
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progress);
        mCurrenciesView = findViewById(R.id.rv_currencies);
        mCurrenciesView.setHasFixedSize(true);
        mCurrenciesView.setLayoutManager(layoutManager);
        adapter = new MainAdapter(this);
        adapter.setHasStableIds(true);

        mCurrenciesView.setAdapter(adapter);

        mViewModelFactory = Injection.provideViewModelFactory(this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CurrencyViewModel.class);
    }

    RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            mViewModel.addMove(currencyList -> {
                CurrencyRate from = currencyList.get(fromPosition);
                currencyList.remove(fromPosition);
                currencyList.add(0, from);
            });
            layoutManager.setSmoothScrollbarEnabled(true);
            layoutManager.scrollToPosition(0);
            try {
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.d("###", e.getMessage());
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        progressBar.setVisibility(View.VISIBLE);
        adapter.registerAdapterDataObserver(adapterDataObserver);
        mDisposable.add(mViewModel.getCurrency().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if(result.size() > 1) {
                        progressBar.setVisibility(View.GONE);
                    }
                    adapter.setData(result);
                }, error -> {
                    if (error instanceof HttpException) {
                        String errorBody = ((HttpException) error).response().errorBody().string();
                        Log.d("###", errorBody);
                    } else {
                        Log.d("###", error.getMessage());
                    }
                }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.unregisterAdapterDataObserver(adapterDataObserver);
        mDisposable.clear();
    }

    @Override
    public void onTextChanged(String base, Double value) {
        mViewModel.currentSum(base, value);
    }
}
