package com.currencyrates.persistence;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

@Database(entities = {CurrencyRate.class}, version = 1)
public abstract class CurrencyDatabase extends RoomDatabase {

    private static volatile CurrencyDatabase INSTANCE;

    public abstract CurrencyDao currencyDao();

    public synchronized static CurrencyDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), CurrencyDatabase.class, "Currencies.db")
                    .addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            ContentValues cv = new ContentValues();
                            cv.put("name", "EUR");
                            cv.put("value", "0.00");
                            db.insert("currencies", SQLiteDatabase.CONFLICT_IGNORE, cv);
                        }
                    })
                    .build();
        }
        return INSTANCE;
    }
}
