package com.currencyrates.persistence;

import android.support.annotation.NonNull;

public class InputRate {

    public String name;

    public Double value;

    public InputRate(@NonNull String name, @NonNull Double value) {
        this.name = name;
        this.value = value;
    }
}
