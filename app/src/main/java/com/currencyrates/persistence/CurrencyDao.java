package com.currencyrates.persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class CurrencyDao {

    @Query("SELECT * FROM currencies")
    public abstract Flowable<List<CurrencyRate>> getCurrencyRate();

    @Transaction
    public void insertCurrency(List<CurrencyRate> rates) {
        deleteAllCurrencies();
        insertAll(rates);
    }

    @Insert
    abstract void insertAll(List<CurrencyRate> rates);

    @Query("DELETE FROM currencies")
    abstract void deleteAllCurrencies();
}
