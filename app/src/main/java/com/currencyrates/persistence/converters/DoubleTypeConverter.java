package com.currencyrates.persistence.converters;


import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;

public class DoubleTypeConverter {

    @TypeConverter
    public String fromDouble(Double decimal) {

        return String.valueOf(decimal);
    }

    @TypeConverter
    public Double toDouble(String decimal) {
        if (TextUtils.isEmpty(decimal)) {
            return 0.00;
        }
        return Double.parseDouble(decimal);
    }
}
