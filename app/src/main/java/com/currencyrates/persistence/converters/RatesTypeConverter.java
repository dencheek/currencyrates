package com.currencyrates.persistence.converters;

import android.arch.persistence.room.TypeConverter;
import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class RatesTypeConverter {

    private static final String SEPARATOR = ":";

    @TypeConverter
    public static Map<String, String> toMap(String rates) {
        if (rates == null) {
            return null;
        }
        StringReader reader = new StringReader(rates);
        JsonReader json = new JsonReader(reader);
        Map<String, String> result = new HashMap<>();
        try {
            json.beginArray();
            while (json.hasNext()) {
                String rate = json.nextString();
                String[] parts = rate.split(SEPARATOR);
                result.put(parts[0], parts[1]);
            }
            json.endArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @TypeConverter
    public static String fromMap(Map<String, String> rates) {
        if (rates == null) {
            return null;
        }
        StringWriter result = new StringWriter();
        JsonWriter json = new JsonWriter(result);
        try {
            json.beginArray();
            for (Map.Entry<String, String> entry : rates.entrySet()) {
                String rate = String.format("%s%s%s", entry.getKey(), SEPARATOR, entry.getValue());
                json.value(rate);
            }
            json.endArray();
            json.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
