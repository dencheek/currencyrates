package com.currencyrates.persistence;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.currencyrates.persistence.converters.DoubleTypeConverter;

@Entity(tableName = "currencies")
public class CurrencyRate {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    public String name;

    @NonNull
    @ColumnInfo(name = "value")
    @TypeConverters(DoubleTypeConverter.class)
    public Double value;

    public CurrencyRate(@NonNull String name, @NonNull Double value) {
        this.name = name;
        this.value = value;
    }

}
