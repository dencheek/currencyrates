package com.currencyrates.util;

public class TimeLimiter {

    private long timestamp = -1;

    private long timeout;

    public TimeLimiter(long timeout) {
        this.timeout = timeout;
    }

    public synchronized boolean shouldFetch() {
        long currentTime = System.currentTimeMillis();
        if (timestamp == -1) {
            timestamp = currentTime;
            return true;
        }
        boolean shouldFetch = (currentTime - timestamp) > timeout;
        if (shouldFetch) {
            timestamp = currentTime;
        }
        return shouldFetch;

    }
}
