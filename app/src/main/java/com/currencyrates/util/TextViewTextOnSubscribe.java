package com.currencyrates.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.Observer;

final class TextViewTextOnSubscribe extends Observable<CharSequence> {

    final TextView view;

    TextViewTextOnSubscribe(TextView view) {
        this.view = view;
    }

    @Override
    protected void subscribeActual(Observer<? super CharSequence> observer) {
        final TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                observer.onNext(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        view.addTextChangedListener(watcher);
        observer.onNext(view.getText());
    }
}
