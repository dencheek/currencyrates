package com.currencyrates.util;

import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;

public final class RxTextView {

    private RxTextView() {
    }

    @NonNull
    public static Observable<CharSequence> textChanges(@NonNull TextView view) {
        return new TextViewTextOnSubscribe(view);
    }
}
