package com.currencyrates.util;

import com.currencyrates.R;

import java.util.HashMap;
import java.util.Map;

public class CurrencyInfoUtil {

    private static final Map<String, Integer> flags = new HashMap<>();

    static {
        flags.put("EUR", R.drawable.eu);
        flags.put("AUD", R.drawable.au);
        flags.put("BGN", R.drawable.bg);
        flags.put("BRL", R.drawable.br);
        flags.put("CAD", R.drawable.ca);
        flags.put("CHF", R.drawable.ch);
        flags.put("CNY", R.drawable.cn);
        flags.put("CZK", R.drawable.cz);
        flags.put("DKK", R.drawable.dk);
        flags.put("GBP", R.drawable.gb);
        flags.put("HKD", R.drawable.hk);
        flags.put("HRK", R.drawable.hr);
        flags.put("HUF", R.drawable.hu);
        flags.put("IDR", R.drawable.id);
        flags.put("ILS", R.drawable.il);
        flags.put("INR", R.drawable.in);
        flags.put("JPY", R.drawable.jp);
        flags.put("KRW", R.drawable.kr);
        flags.put("MXN", R.drawable.mx);
        flags.put("MYR", R.drawable.my);
        flags.put("NOK", R.drawable.no);
        flags.put("NZD", R.drawable.nz);
        flags.put("PHP", R.drawable.ph);
        flags.put("PLN", R.drawable.pl);
        flags.put("RON", R.drawable.ro);
        flags.put("RUB", R.drawable.ru);
        flags.put("SEK", R.drawable.se);
        flags.put("SGD", R.drawable.sg);
        flags.put("THB", R.drawable.th);
        flags.put("TRY", R.drawable.tr);
        flags.put("USD", R.drawable.us);
        flags.put("ZAR", R.drawable.za);
    }

    private static final Map<String, String> names = new HashMap<>();

    static {
        names.put("EUR", "European euro");
        names.put("AUD", "Australian dollar");
        names.put("BGN", "Bulgarian lev");
        names.put("BRL", "Brazilian real");
        names.put("CAD", "Canadian dollar");
        names.put("CHF", "Swiss franc");
        names.put("CNY", "Chinese Yuan Renminbi");
        names.put("CZK", "Czech koruna");
        names.put("DKK", "Danish krone");
        names.put("GBP", "Pound sterling");
        names.put("HKD", "Hong Kong dollar");
        names.put("HRK", "Croatian kuna");
        names.put("HUF", "Hungarian forint");
        names.put("IDR", "Indonesian rupiah");
        names.put("ILS", "Israeli new shekel");
        names.put("INR", "Indian rupee");
        names.put("JPY", "Japanese yen");
        names.put("KRW", "South Korean won");
        names.put("MXN", "Mexican peso");
        names.put("MYR", "Malaysian ringgit");
        names.put("NOK", "Norwegian krone");
        names.put("NZD", "New Zealand dollar");
        names.put("PHP", "Philippine peso");
        names.put("PLN", "Polish zloty");
        names.put("RON", "Romanian leu");
        names.put("RUB", "Russian ruble");
        names.put("SEK", "Swedish krona");
        names.put("SGD", "Singapore dollar");
        names.put("THB", "Thai baht");
        names.put("TRY", "Turkish lira");
        names.put("USD", "United States dollar");
        names.put("ZAR", "South African rand");
    }

    public static Map<String, String> getNames() {
        return names;
    }

    public static Map<String, Integer> getFlags() {
        return flags;
    }
}
