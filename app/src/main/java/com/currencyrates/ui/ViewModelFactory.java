package com.currencyrates.ui;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.currencyrates.repository.CurrencyRepository;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final CurrencyRepository mRepo;

    public ViewModelFactory(CurrencyRepository mDataSource) {
        this.mRepo = mDataSource;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(CurrencyViewModel.class)) {
            return (T) new CurrencyViewModel(mRepo);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
