package com.currencyrates.ui;

import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.currencyrates.persistence.CurrencyRate;
import com.currencyrates.persistence.InputRate;
import com.currencyrates.repository.CurrencyRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;

public class CurrencyViewModel extends ViewModel {

    private final CurrencyRepository mRepo;

    private static final List<Consumer<List<CurrencyRate>>> moves = new ArrayList<>();

    public CurrencyViewModel(CurrencyRepository mDataSource) {
        this.mRepo = mDataSource;
    }

    public Flowable<List<CurrencyRate>> getCurrency() {
        return mRepo.loadCurrency()
                .map(rates -> {
                    Double coeff = 1.00;
                    try {
                        InputRate inputRate = mRepo.getInput();
                        Double c = mRepo.getCurrentRates().get(inputRate.name).doubleValue();
                        coeff = inputRate.value / c;
                    } catch (Exception e) {
                        Log.d("###", e.getMessage());
                    }
                    List<CurrencyRate> result = new ArrayList<>();
                    for (CurrencyRate rate : rates) {
                        Double value = rate.value;
                        CurrencyRate currencyRate = new CurrencyRate(rate.name, value * coeff);
                        result.add(currencyRate);
                    }
                    return result;
                })
                .map(result -> {
                    for (Consumer<List<CurrencyRate>> c : moves) {
                        c.accept(result);
                    }
                    CurrencyRate first = result.get(0);
                    result.set(0, new CurrencyRate(first.name, mRepo.getInput().value));
                    return result;
                });
    }

    public void addMove(Consumer<List<CurrencyRate>> move) {
        moves.add(move);
    }

    public void currentSum(String base, Double coefficient) {
        mRepo.insertInput(new InputRate(base, coefficient));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepo.dispose();
    }
}
